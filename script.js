'use strict';

let images = document.querySelectorAll(".image-to-show");
let stopBtn = document.getElementById("stop-btn");
let startBtn = document.getElementById("start-btn");
let currentIndex = 0;
let intervalID;

function showNextImage() {
  images[currentIndex].classList.remove("visible");
  currentIndex = (currentIndex + 1) % images.length;
  images[currentIndex].classList.add("visible");
}

function startSlideShow() {
  images[currentIndex].classList.add("visible");
  intervalID = setInterval(showNextImage, 3000);
}

function stopSlideShow() {
  clearInterval(intervalID);

  for (let i = 0; i < images.length; i++) {
    if (i !== currentIndex) {
      images[i].classList.remove("visible");
    }
  }
}

stopBtn.addEventListener("click", stopSlideShow);
startBtn.addEventListener("click", startSlideShow);

startSlideShow();